import React, {useState} from 'react';
import ReactDOM from 'react-dom';
import './index.css';

class GameLogic {
  static calculateWinnerStatus(squares) {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (squares[a]
          && squares[a] === squares[b]
          && squares[a] === squares[c]) {
        return {winner: squares[a], row: lines[i]};
      }
    }
    return null;
  }
}

function Square(props) {
  return (
      <button
          className={'square ' + props.className}
          onClick={props.onClick}>
        {props.value}
      </button>
  );
}

function Board(props) {

  function renderSquare(i) {
    const winRow = props.winnerRow;
    const className = (winRow && winRow.includes(i)) ? 'victoryCell' : '';

    return <Square
        className={className}
        value={props.squares[i]}
        onClick={() => props.onClick(i)}
    />;
  }

  const content = [];
  let j = 0;
  for (let i = 0; i < 3; i++) {
    const block = [];
    for (let k = 0; k < 3; k++) {
      block.push(renderSquare(j));
      j++;
    }
    content.push(<div className="board-row">{block}</div>);
  }
  return content;
}

function Game() {
  const [history, setHistory] = useState([{
    squares: Array(9).fill(null),
    location: undefined,
  }]);

  const [isNextCross, setNextCross] = useState(false);
  const [stepNumber, setStepNumber] = useState(0);
  const [sortMovesDescending, setSortMovesDescending] = useState(false);

  function clickCellUpdateState(squaresCopy, index) {

    const newHistory = history.slice();
    newHistory.push({
          location: index,
          squares: squaresCopy,
        },
    );
    setHistory(newHistory);
    squaresCopy[index] = isNextCross ? 'O' : 'X';
    setNextCross(!isNextCross);
    setStepNumber(stepNumber + 1);
  }

  function handleClick(index) {
    if (stepNumber !== (history.length - 1)) {
      return;
    }
    const lastSquares = history[history.length - 1].squares;
    if (lastSquares[index] === null) {

      //
      // prevent further clicks in case there's a winner
      //
      if (GameLogic.calculateWinnerStatus(lastSquares)) {
        return;
      }
      clickCellUpdateState(lastSquares.slice(), index);
    }
  }

  function jumpTo(move) {
    setStepNumber(move);
    setNextCross((move + 1) % 2 === 0);
    // TODO Multiple State change
  }

  function renderMoves(winnerStatus) {
    return history.map((step, move) => {
      const description = move ? 'Go to move #' + move : 'Go to start';
      let locationString;
      if (step.location !== undefined && step.location !== null) {
        const row = Math.floor(step.location / 3);
        const column = (step.location % 3);
        locationString = `( ${row},${column} )`;
      } else {
        locationString = '';
      }
      const player = (move % 2) === 0 ? 'O' : 'X';
      const moveCssClass = move === stepNumber ? 'selectedMove' : '';

      return (<li key={move} className={moveCssClass}>
        <span>{move !== 0 ? player : ''} {locationString}</span>
        <button onClick={() => jumpTo(move)}>{description}</button>
      </li>);
    });
  }

  function calculateStatus(winnerStatus) {
    if (winnerStatus) {
      console.log({winnerStatus});
      return 'Winner: ' + winnerStatus.winner;
    } else if (history.length === 10) {
      return 'Draw: no winner';
    } else {
      return 'Next player: ' + (isNextCross ? 'O' : 'X');
    }
  }

  function changeSorting() {
    setSortMovesDescending(!sortMovesDescending);
  }
  console.log({history});
  const step = history[history.length - 1];
  const winnerStatus = GameLogic.calculateWinnerStatus(step.squares);

  const status = calculateStatus(winnerStatus);
  const moves = renderMoves(winnerStatus);

  const current = history[stepNumber];

  const sortSymbol = sortMovesDescending === true
      ? '\u2B06'
      : '\u2B07';
  if (sortMovesDescending) {
    moves.reverse();
  }
  return (
      <div className="game">
        <div className="game-board">
          <Board
              squares={current.squares}
              onClick={index => handleClick(index)}
              winnerRow={winnerStatus ? winnerStatus.row : []}/>
        </div>
        <div className="game-info">

          <div>
            <button className='buttonIcon' onClick={() => changeSorting()}>
              <span>{sortSymbol}</span>sort
            </button>
            {status}</div>
          <ul>{moves}</ul>
        </div>
      </div>
  );
}

ReactDOM.render(
    <Game/>,
    document.getElementById('root'),
);
